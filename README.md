# README - api-ansible

## WARNING: NOW WITH SUBMODULES!!!

`roles/devops-role-barricade` is included as a submodule in the api-ansible git repository.  If you find yourself wanting to change it or update to a newer version:

* Consult the DevOps team
* Read up on git submodules

## Building Development 

### PHP (v3) Dev

The Vagrantfile controls the invocations of ansible-playbook

Try `vagrant up` in the api-main-php directory

### Node (v2) Dev

* api-main: `vagrant global-status` to double-check your current state
* api-main: Run `vagrant up` in api-main to create or start your VM if necessary

## Building Integration

### Playbook: autocolor-api-v2-node.yaml
### Playbook: autocolor-api-v3-php.yaml

These playbooks will create instances and provision them, while leaving "breadcrumbs" about the color state.

If you are running these playbooks locally, please manually create the breadcrumb directory `/etc/ansiblebs` on your machine.

### Utility: utilities/reset-ansible-ec2-cache.sh 

Run this shell script to nuke your ec2.py cache and force ec2.py to read the latest information on your instances from AWS.  If you don't, sometimes the machines you create won't be in your Ansible Inventory.

### Playbook: autocolor-api-lb-swap.yaml

The autocolor-api-lb-swap.yaml playbook will use the "breadcrumbs" to either activate the deploy or backout the deploy by moving old and new instances in and out of the load balancer.

## Tricks and Traps

#### ec2 Inventory

tag_role_api-search-zookeeper = All Zookeeper instances
tag_role_api-search-solr = All Solr instances

#### Different EXPORT variables for Boto vs Ansible

Boto looks for specifically:
```
    export AWS_ACCESS_KEY_ID='AK123'
    export AWS_SECRET_ACCESS_KEY='abc123'
```

Ansible can also use (and does use in some examples)
```
    export AWS_ACCESS_KEY=xxx
    export AWS_SECRET_KEY=xxx
```

...but these shorter environment variables won't work for Boto!  Use the Boto version!

## Rebuilding Jenkins and its nodes

### Jenkins Master

`api-ansible/MPGtest-jenkins-master.yaml`

This playbook is not yet complete.  

### Jenkins nodejs Servant (jenkinsservant)

Here is the job as it appears on the Jenkins master to stand up a new nodejs Servant.

```
cd ${WORKSPACE}/api-ansible
chmod 0600 secrets/*

# 1. Refresh ec2 cache
hosts/ec2.py --refresh-cache > /dev/null

# 2. Build new jenkins nodejs servant
ansible-playbook \
autocolor-api-jenkinsservant.yaml \
-i hosts/ec2.py \
--user=ubuntu \
--private-key=secrets/sage2.pem \
--extra-vars "instance_count=1 instance_type=m3.medium hosts_tag=autocolor wfm_team=api wfm_environment=shared wfm_application=jenkinsservant node_environment=integrationb" \
--vault-password-file ~/.vault_pass.txt

# 3. Refresh cache again
hosts/ec2.py --refresh-cache > /dev/null

# 4. Destroy previous servant
ansible-playbook ec2-terminate-autocolor.yaml -i hosts/ec2.py \
--extra-vars "wfm_team=api wfm_environment=shared wfm_application=jenkinsservant wfm_misc_color=auto" \
--user=ubuntu \
--private-key=secrets/sage2.pem

```

### Jenkins PHP Servant (jenkinsphpservant)

Here is the job as it appears on the Jenkins master to stand up a new PHP Servant.

```

cd ${WORKSPACE}/api-ansible
chmod 0600 secrets/*

# 1. Refresh ec2 cache
hosts/ec2.py --refresh-cache > /dev/null

# 2. Build new jenkins php servant
ansible-playbook \
autocolor-api-jenkinsphpservant.yaml \
-i hosts/ec2.py \
--user=ubuntu \
--private-key=secrets/sage2.pem \
--extra-vars "instance_count=1 instance_type=m3.medium hosts_tag=autophpservant wfm_team=api wfm_environment=shared wfm_application=jenkinsphpservant php_environment=integration" \
--vault-password-file ~/.vault_pass.txt

# 3. Refresh cache again
hosts/ec2.py --refresh-cache > /dev/null

# 4. Destroy previous servant
ansible-playbook ec2-terminate-autocolor.yaml -i hosts/ec2.py \
--extra-vars "wfm_team=api wfm_environment=shared wfm_application=jenkinsphpservant wfm_misc_color=auto" \
--user=ubuntu \
--private-key=secrets/sage2.pem

```

## General Ansible Notes

### Host Key Changed Errors

We used to use ANSIBLE_HOST_KEY_CHECKING=False but we now instead rely on host_key_checking=false in ansible.cfg

If attempting to SSH to a rebuilt host, try deleting the entry in ~/.ssh/known_hosts

### The hosts: line convention

Creating playbooks with a variable as the host group to target allows us to use either dynamic or static host inventory files; just be sure to give examples.

```
- include: includes/playbooks/check-ansible-version.yaml
- include: includes/playbooks/check-hosts-variables.yaml

- name: "my-playbook-name.yaml"

  hosts: '{{ ["tag_wfm_team_",wfm_team,":&","tag_wfm_environment_",wfm_environment,":&","tag_wfm_application_",wfm_application,":&","tag_wfm_misc_color_",wfm_misc_color]|join("") if ( (wfm_team is defined) and (wfm_environment is defined) and (wfm_application is defined) and (wfm_misc_color is defined) ) else hosts_tag }}' # "

```
is the current standard header - it will check to make sure the proper variables are present to construct a hosts: line, and then construct it.

## Search Infrastructure Notes

These playbooks were Proof-Of-Concept and are not currently in service.

We assume api-search is already a keypair in AWS, and that secrets/api-search.pem matches.

### Setup SG and Instances

```
ansible-playbook apisearch-ec2-security-group.yaml -i hosts/local-binpy.ini
ansible-playbook ec2-launch-zookeepers.yaml -i hosts/local-binpy.ini
ansible-playbook ec2-launch-solrs.yaml -i hosts/local-binpy.ini
```

### Verify results

```
hosts/ec2.py --list | grep "tag_role_api-search-zookeeper" -A 4
hosts/ec2.py --list | grep "tag_role_api-search-solr" -A 2
```

### Buildouts 

```
ansible-playbook ec2-buildout-zookeepers.yaml -i hosts/ec2.py -u ubuntu --private-key secrets/api-search.pem --vault-password-file ~/.vault_pass.txt

ansible-playbook ec2-buildout-solrs.yaml -i hosts/ec2.py -u ubuntu --private-key secrets/api-search.pem --vault-password-file ~/.vault_pass.txt
```

