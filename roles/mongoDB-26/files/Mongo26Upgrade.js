// This file reforms the Mongod authentication method for Mongo2.6
db.upgradeCheckAllDBs() //check
db.getSiblingDB("admin").runCommand({authSchemaUpgrade: 1 }); //Upgrade Authentication method for Mongo2.6
