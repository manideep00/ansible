Using Bitbucket OAUTH Plugin for Jenkins
========================================

First you need to get consumer key/secret from Bitbucket.

* Log into your Bitbucket account.
* Click accountname > Manage Account from the menu bar. The Account settings page appears.
* Click Access Management, OAuth from the menu bar.
* Click the Add consumer button.
* The system requests the following information: Name is required. Others are optional.
* Press Add consumer. The system generates a key and a secret for you. Toggle the consumer name to see the generated Key and Secret value for your consumer.


Key	pjF6jxdUYEUfaaLxAs
Secret	nzgdekrNz65L83Jbrbn7gav7D2Mj7SWU

Second, you need to configure your Jenkins.

* Open Jenkins Configure System page.
* Set correct URL to Jenkins URL.
* Click Save button.
* Open Jenkins Configure Global Security page.
* Check Enable security.
* Select Bitbucket OAuth Plugin in Security Realm.
* Input your Consumer Key to Client ID.
* Input your Consumer Secret to Client Secret.
* Click Save button.
