
# TODO: DO WE NEED A SHEBANG LINE FOR THIS SCRIPT FILE?
# TODO: FIND OUT WHY ./sage-node-tasks.sh deploy test FREEZES ON GIT CHECKOUT STEP

# Preconditions:
#   The JSON config is present and contains ALL extra-vars needed to launch/build/deploy
#   ( see json_vars/node-production-vars.json and json_vars/node-test-vars.json )

# Description:
#   Braxton's excellent first attempt at making the production deploy process simpler and more repeatable

# Examples:
#   ./sage-node-tasks.sh launch {test|production}
#   ./sage-node-tasks.sh build {test|production}
#   ./sage-node-tasks.sh deploy {test|production}

# TODOs:
#   Move the json files out of the root directory
#   Think deeply about how we will manage the changing variables over time in *.json
#   Consider making git_tag a command line parameter
#   Consider auto-creating the counter_tag by default
#   Check for second command-line variable ($2)
#   Consider swapping $1 and $2 ?

# WARNINGS:
#   THIS FILE IS NO LONGER VALID
#   IT IS BEING KEPT AS A PLACEHOLDER FOR FUTURE WORK AND...
#   A REMINDER TO USE THE JSON_VARS DIR

inventory_file=hosts/ec2.py

case "$1" in

check) echo "Double-checking config JSON..."
  cat json_vars/node-$2-vars.json
  ;;

launch) echo "Launching..."
  ansible-playbook ec2-launch-instance.yaml \
  --inventory-file=$inventory_file \
  --vault-password-file ~/.vault_pass.txt \
  --extra-vars "@json_vars/node-$2-vars.json"
  # --extra-vars "counter_tag=$counter_tag server_name=$server_name role=$role instance_count=$instance_count instance_type=$instance_type"
  ;;

build) echo "Building..."
  ansible-playbook \
  app-servers.yaml --inventory-file=$inventory_file \
  --user=ubuntu \
  --private-key=secrets/sage2.pem \
  --vault-password-file ~/.vault_pass.txt \
  --extra-vars "@json_vars/node-$2-vars.json"
  ;;

deploy) echo "Deploying..."
  ansible-playbook deploy-sage.yaml \
  --inventory-file=$inventory_file \
  --user=ubuntu \
  --private-key=secrets/sage2.pem \
  --extra-vars "@json_vars/node-$2-vars.json"
  ;;

*) echo "Task type not specified. Must be launch, build, deploy.  '$1' was passed"
  ;;

esac

# NOT VALIDATED
