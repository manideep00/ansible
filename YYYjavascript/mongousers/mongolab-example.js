//////
//
// file: ....ansible/YYYjavascript/mongolab-example.js
//
////// REPLACE:
//////   "98765" with your MongoLab Port (should correspond to the ds/rs)
//////   "-example" with your app and environment (i.e. sage-integration)
//////   YourAdminUser with your admin user
//////   Fake Things with Real Things
//
// usage: mongo ds098765-a0.mongolab.com:98765 -u YourAdminUser -p [REDACTED] --authenticationDatabase admin mongolab-example.js
// or: mongo --host rs-ds098765/ds098765-a1.mongolab.com:98765,ds098765-a0.mongolab.com:98765 -u YourAdminUser -p [REDACTED] --authenticationDatabase admin mongolab-example.js
// 
// Ensures dbs "firebird", "paprika_test", and "paprika_qa" exist with the proper users.
//
// preconditions: 
//	* an admin user already exists in the admin database
//
// todo:
//  * !! use loops, arrays instead of copypasta
//  * consider expanding this into an intelligent "create the collections and base data" script
//  * consider collecting all users and reporting if any strangers are detected
//  * consider making this a template to grab creds from a Single Source, then
//      using a playbook to generate and perhaps run the actual javascript.
////

////
// DATABASE: FIREBIRD
////

db = db.getSiblingDB('firebird');

var uaFind  = db.system.users.find({user:"exampleuser"});

if (uaFind.hasNext()) {
	db.changeUserPassword("exampleuser", "12345678");
	} else {
	db.addUser( { user: "exampleuser", pwd: "12345678", roles: ["read"] } );
	}

var ubFind  = db.system.users.find({user:"firebirdAdmin"});

if (ubFind.hasNext()) {
	db.changeUserPassword("firebirdAdmin", "aaaabbbbcccc");
	} else {
	db.addUser( { user: "firebirdAdmin", pwd: "aaaabbbbcccc", roles: ["readWrite,dbAdmin"] } );
	}

var ucFind  = db.system.users.find({user:"firebirdUser"});

if (ucFind.hasNext()) {
	db.changeUserPassword("firebirdUser", "abcd1234");
	} else {
	db.addUser( { user: "firebirdUser", pwd: "abcd1234", roles: ["readWrite"] } );
	}

////
// DATABASE: PAPRIKA_TEST
////

db = db.getSiblingDB('paprika_test');

var udFind  = db.system.users.find({user:"chlorine"});

if (udFind.hasNext()) {
	db.changeUserPassword("chlorine", "corn");
	} else {
	db.addUser( { user: "chlorine", pwd: "corn", roles: ["readWrite"] } );
	}

var uarFind  = db.system.users.find({user:"anIronWorkerName"});

if (uarFind.hasNext()) {
	db.changeUserPassword("anIronWorkerName", "GetLeadOut");
	} else {
	db.addUser( { user: "anIronWorkerName", pwd: "GetLeadOut", roles: ["read"] } );
	}

var ulwFind = db.system.users.find({user:"consultingCo"});

if (ulwFind.hasNext()) {
	// no op; don't reset password
	// just ensure that the user exists
	} else {
	db.addUser( { user: "consultingCo", pwd: "jebdidnotprovideone", readOnly: true, roles: ["read"] })
	}
}

////
// DATABASE: PAPRIKA_QA
// Requested by Clove Leachman
////

db = db.getSiblingDB('paprika_qa');

var ueFind  = db.system.users.find({user:"paprikaQaAdmin"});

if (ueFind.hasNext()) {
	db.changeUserPassword("paprikaQaAdmin", "nootmegg");
	} else {
	db.addUser( { user: "paprikaQaAdmin", pwd: "nootmegg", roles: ["readWrite,dbAdmin"] } );
	}

var ufFind  = db.system.users.find({user:"paprikaQaUser"});

if (ufFind.hasNext()) {
	db.changeUserPassword("paprikaQaUser", "moarnootmegg");
	} else {
	db.addUser( { user: "paprikaQaUser", pwd: "moarnootmegg", roles: ["readWrite"] } );
	}

// EOF : mongolab-example.js
//////
