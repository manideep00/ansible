---
# file: api-ansible/api-workers-php.yaml

# Description:
#   From an existing PHP server, deploy the workers.

# Preconditions:
#   The PHP server must have the code we wish to deploy present.
#   The PHP server must have the ironcli role (installs the iron cli)

# Required Vars:
#   php_environment - php environment (integration, qa, etc)
# to pass to the Symfony console

# Examples:

# ansible-playbook \
# api-workers-php.yaml \
# -i hosts/ec2.py \
# --user=ubuntu \
# --private-key=secrets/sage2.pem \
# --extra-vars "wfm_team=api wfm_environment=integration wfm_application=sagev3php wfm_misc_color=puce php_environment=integration"

# TODOs:
#   Add an option to update the code, perhaps chain an include to the 
# api-v3-php playbook if the update_code tag is set
#   Make this playbook work with autocoloration

# WARNINGS:
#   The run_once directive ensures we only run on one host instead of
# deploying the workers multiple times (one for each app server).  Keep
# your eyes open when modifying this playbook and testing.
#   *** HOWEVER *** when you register the result of a run_once, that 
# variable is the same for all hosts going forward, magically.

- include: includes/playbooks/check-ansible-version.yaml
- include: includes/playbooks/check-hosts-variables.yaml

- name: "api-workers-php.yaml"

  hosts: '{{ ["tag_wfm_team_",wfm_team,":&","tag_wfm_environment_",wfm_environment,":&","tag_wfm_application_",wfm_application,":&","tag_wfm_misc_color_",wfm_misc_color]|join("") if ( (wfm_team is defined) and (wfm_environment is defined) and (wfm_application is defined) and (wfm_misc_color is defined) ) else hosts_tag }}' # "

  vars:
    wfm_pb_name: "api-workers-php.yaml"
    nginx_user: www-data
    pb_required_vars:
      - php_environment

  pre_tasks:
    - include: includes/notify-sumo.yaml
      wfm_playbook: "{{ wfm_pb_name }}"
      wfm_state: "START"

    - include: includes/check-pb-required-vars.yaml
      tags: always

    - name: Check for invalid value of variable wfm_application
      fail: msg="Variable wfm_application may be set incorrectly - expecting sagev3php"
      when: wfm_application is defined and wfm_application != 'sagev3php'
      tags: always

  tasks:
    # Use single quotes around the shell argument
    # and 2x single quotes around the star so it survives
    # https://github.com/ansible/ansible/issues/6006
    - name: Deploy PHP workers only once using run_once
      sudo: True
      shell: 'php /usr/share/nginx/html/app/console worker:deploy -w ''*'' --env={{ php_environment }}'
      run_once: true
      register: worker_run
      tags: deployworkers

    # - name: debug 0
    #   debug: var=worker_run

  post_tasks:
    - include: includes/notify-sumo.yaml
      wfm_playbook: "{{ wfm_pb_name }}"
      wfm_state: "END"
      when: worker_run|success
      tags: always

    - include: includes/notify-sumo.yaml
      wfm_playbook: "{{ wfm_pb_name }}"
      wfm_state: "FAIL"
      when: worker_run|failed
      tags: always

    - name: Notify Hipchat of the PHP Worker Deploy
      hipchat:
        token={{ default_hipchat_token }}
        room={{ default_hipchat_room }}
        from={{ default_hipchat_from }}
        msg='Just deployed All PHP Workers to {{ php_environment }}'
        validate_certs=no
      ignore_errors: yes   # DO NOT FAIL IF HIPCHAT IS DOWN
      run_once: true
      delegate_to: 127.0.0.1
      when: worker_run|success
      tags: always

