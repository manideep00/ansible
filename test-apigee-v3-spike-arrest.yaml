---
# file: api-ansible/test-apigee-v3-spike-arrest.yaml

# Description:
#   Spam the v3 users profile endpoint until it cries

# Tower Jobs:
#   (none)
#   If you don't list them, you don't get to complain if/when they break

# Preconditions:
#   Localhost has libraries needed to curl etc
#   Vault password has been passed in for the secrets
#     wfm_apigee_devapptwo_testkey
#     wfm_apigee_devapptwo_testsecret
#   _testkey should have oauth access and access to the products

# Required Vars:
#   wfm_environment   # integration or qa - production will have diff keys

# Examples:
# ansible-playbook \
# test-apigee-v3-spike-arrest.yaml \
# -i hosts/local-binpy.ini \
# --extra-vars "wfm_environment=integration" \
# --vault-password-file ~/.vault_pass.txt \
# --forks=5

# TODOs:
#   Make the capture of the oauth tokens more robust

# WARNINGS:
#   1. THIS PLAYBOOK ONLY DESIGNED TO TEST AGAINST INTEGRATION OR QA
# PRODUCTION WILL HAVE DIFFERENT PRODUCTS THUS DIFFERENT KEYS
#   2. Now that spike arrest limit is set to 5 transactions per second,
# we probably can't trigger it with a simple playbook anymore.  I'm
# leaving this in place partially to see if Ansible v2.0 will make it 
# possible, partially just as a curiosity.

- include: includes/playbooks/check-ansible-version.yaml

- name: "Slam v3 users profile"

  hosts: 127.0.0.1     # if local-only
  connection: local    # if local-only
  gather_facts: false  # if local-only

  vars:
    wfm_pb_name: "test-apigee-v3-spike-arrest.yaml"
    pb_required_vars:
      - wfm_environment 
    v3_test_profile: "/v3/users/profile/"

  pre_tasks:
    - include: includes/notify-sumo.yaml
      wfm_playbook: "{{ wfm_pb_name }}"
      wfm_state: "START"
    - include: includes/check-pb-required-vars.yaml
    - include_vars: secrets/apigee-devapp-keys.yaml

  tasks:
    - name: Display target environment
      debug:
        msg="wfm_environment is {{ wfm_environment }}"
      no_log: false

    - name: Prepare helper fact
      set_fact:
        oauther: "{{ wfm_apigee_urls[wfm_environment] }}/oauth20/token?grant_type=client_credentials&client_id="

    - name: Oauth with good key and secret
      uri:
        url: "{{ oauther }}{{ wfm_apigee_devapptwo_testkey }}&client_secret={{ wfm_apigee_devapptwo_testsecret }}"
        method: GET
        return_content: yes
      register: uriresult
      failed_when: "'<token>' not in uriresult.content"

    - name: Capture the Good Token
      set_fact:
        good_token: "{{ uriresult.content | regex_replace('^.*<token>','') | regex_replace('..token>.*','') | trim}}"

    - name: debug
      debug: msg="{{ wfm_apigee_urls[wfm_environment] }}{{v3_test_profile}}0?&access_token={{good_token}}"

    - name: Pause to be polite to apigee and the network
      pause: seconds=5

# Our profiles do not exist, so the expected result is 500
# Spike arrest calls will appear as errors

# This solution is not ideal, we can't fire as fast as we would really like
# Ansible v2.0 will allow with_* to run async, which might help
# We probably want to use a shell or python script instead (Locust.py?)

# Adding weight: 2 to trigger the 5 per second on some calls hopefully
# weight: 1 never seems to trigger it, 3 or greater triggers 100%
    - name: Spam as hard as we can
      uri:
        url: "{{ wfm_apigee_urls[wfm_environment] }}{{v3_test_profile}}{{item}}?&access_token={{good_token}}"
        method: GET
        HEADER_weight: "2"
        status_code: 500
        return_content: yes
      register: spamresults
      ignore_errors: yes
      with_sequence: count=10

    # - name: debug again
    #   debug: var=spamresults

  post_tasks:
    - include: includes/notify-sumo.yaml
      wfm_playbook: "{{ wfm_pb_name }}"
      wfm_state: "END"
      tags: always

    - include: includes/notify-hipchat.yaml
      vars:
        what_deployed: "Apigee spikearrest testing for {{ wfm_environment }}"
        what_deployed_version: "v3 regular endpoints"
      tags: always

# VALIDATED: MPG 2016-FEB-11