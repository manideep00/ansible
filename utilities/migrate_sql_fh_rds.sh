#!/bin/bash


if [ "$production" == "true" ] || [ "$production" == "false" ]
then
  # echo $production
  if [[ "$production" == "true" ]]; then
    echo "Migrating production data..."

    # RDS SAM/CARL prod
    RDS_HOST='carl-sam-production.c2ztmofymdna.us-east-1.rds.amazonaws.com'
    RDS_USER='root'
    RDS_PASS='J8i4RUdM6LFfoL3y'

    # FH SAM/CARL prod
    FH_HOST='204.13.110.17'
    FH_USER='sage'
    FH_PASS='l1berate0urdata'
  else
    echo "Migrating NON-production data..."

    # RDS SAM/CARL dev
    RDS_HOST='carl-sam-development.c2ztmofymdna.us-east-1.rds.amazonaws.com'
    RDS_USER='root'
    RDS_PASS='rJwFpZw6u8AU3YUarzgL'

    # FH SAM/CARL dev
    FH_HOST='204.13.110.13'
    FH_USER='sageAdminSSL'
    FH_PASS='rap1dd3v!!'
  fi
else
  echo "Must set production to true or false:"
  echo "example: production=false ./migrate_sql_fh_rds.sh"
  exit
fi

FH_PORT='3306'
RDS_PORT='3306'
BAKUP_FILE=/tmp/backup.$(date '+%d-%b-%Y').sql

echo -e "Dumping a mysql database will take some time, but we encourage you to wait patiently"
mysqldump \
--user=$FH_USER \
--host=$FH_HOST \
--password=$FH_PASS \
--single-transaction \
--skip-add-locks \
--order-by-primary \
--routines \
--add-drop-database \
--databases sam lod_recipes store_locations \
> $BAKUP_FILE

# gift_card
# lists
# lod_api
# lod_recipes_test
# sam_test
# test
# tm_discount
# wfm_myrecipes
# wfm_recipes

DB_TOTAL_SIZE=`du -sh $BAKUP_FILE`
DB_LINE_COUNT=`cat $BAKUP_FILE | wc -l`
echo -e "$BAKUP_FILE is \n $DB_LINE_COUNT lines long and \n $DB_TOTAL_SIZE \n\n"

echo "Restoring the databases..." ;
cat $BAKUP_FILE | mysql --user=$RDS_USER --host=$RDS_HOST --password=$RDS_PASS;
